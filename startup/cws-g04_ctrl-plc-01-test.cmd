
# @field REQUIRE_g04waterthreatment-plc_VERSION
# @runtime YES

# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_g04waterthreatment-plc_PATH
# @runtime YES

# Load plc interface database
dbLoadRecords("g04waterthreatment-plc-test.db", "MODVERSION=$(REQUIRE_g04waterthreatment-plc_VERSION)")

# Configure autosave
# Number of sequenced backup files to write
save_restoreSet_NumSeqFiles(1)

# Specify directories in which to search for request files
set_requestfile_path("$(REQUIRE_g04waterthreatment-plc_PATH)", "misc")

# Specify where the save files should be
set_savefile_path("$(SAVEFILE_DIR)", "")

# Specify what save files should be restored
set_pass0_restoreFile("g04waterthreatment-plc-test.sav")

# Create monitor set
doAfterIocInit("create_monitor_set('g04waterthreatment-plc-test.req', 1, '')")
